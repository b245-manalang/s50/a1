import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Container, Row, Col} from 'react-bootstrap';

export default function CourseCard(){
	return(
		<Container className = 'mt-3'>
	 		<Row>
				<Col>
			 		<Card>
			      
				      <Card.Body>
				        <Card.Title>Sample Course</Card.Title>
				        <Card.Text>

				        <h6>  Description:
				        </h6>
				        <p>
				          This is a sample course offering.
				        </p>

				       	<h6>
				          Price:
				        </h6>
				        <p>
				          PhP 40,000
				        </p>

				        </Card.Text>
				        <Button variant="primary">Enroll</Button>
				      
				      </Card.Body>	
				    </Card>
			    </Col>
		    </Row>
		</Container>
	    
	  );
	}